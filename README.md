# Analyzing the Impact of Workloads on Modeling the Performance of Configurable Software Systems
 This is the companion repository to the ICSE 2023 submission #177 **Analyzing the Impact of Workloads on Modeling the Performance of Configurable Software Systems**. 
 We provide in this repository as supplementary material including experiment data (configuration samples, subject system code and descriptions) as well as a dashboard for interactive
 data exploration. The dashboard enables the viewer to reenact the analyses from our empirical study and investigate in detail vidualizations left out due to space limitations.

 In addition, we provide in a separate [cloud folder](https://mega.nz/folder/8JQxEQAJ#vIWWpDpLkmVCi9RW711ByA) resources left out due to quota limitations of both `HotCRP.com` and `Gitlab.com`. This includes all workload files used in our study
 as well as the raw code coverage data from our experiments.
 

 ### Contents
 * Measurement and Experiment description/files (below)
 * Interactive Dashboard (either self-hosted via Docker or deployed on Heroku)
 * [Cloud folder](https://mega.nz/folder/8JQxEQAJ#vIWWpDpLkmVCi9RW711ByA) with the **workloads** (3.5 GiB) used and **raw coverage data** (3.5 GiB, uncompressed ca. 60GiB)
 
## Interactive Dashboard

We provide an interactive dashboard using streamlit that allows exploring in detail our analysis for each configuration option and workload. We have deployed a version on [Heroku](https://workload-performance.heropkuapp.com) and provide a Docker-ized version to run the dashboard locally (which might perform better).


### Option 1: Running locally
```bash
cd ./dashboard
sudo docker build -t streamlitapp:latest .
docker run -p 8501:8501 streamlitapp:latest
```

You can now explore the dashboard running locally at [https://127.0.0.1:8501](https://127.0.0.1:8501).

### Option 2: Heroku
You can find the deployed version [here](https://workload-performance.herokuapp.com).


### Example Views
<img src="images/rq1.png"  width="330"><img src="images/rq2.png"  width="330"><img src="images/rq3.png"  width="330">

## Study Files

| Name  |  Domain |  Language | Repository  |  Code Used  | Sample [CSV]  |   Performance Measurements [CSV]  | Option-Code [CSV]| 
|---|---|---|---|---|---|---|---|
|  `jump3r` |  Audio Encoder |  Java |  [`Link`](https://github.com/Sciss/jump3r) | [`v1.0.4`](misc/code/jump3r)  |[`Sample`](dashboard/resources/jump3r/sample.csv)  | [`Performance`](dashboard/resources/jump3r/measurements.csv)  | [`Option-Code`](dashboard/resources/jump3r/code/) |
|  `kanzi` |  File Compressor  | Java  | [`Link`](https://github.com/flanglet/kanzsample.csv)  | [`v1.9`](misc/code/kanzi)  | [`Sample`](dashboard/resourceskanzisample.csv)  |[`Performance`](dashboard/resources/kanzi/measurements.csv)  | [`Option-Code`](dashboard/resources/kanzi/code/) |
|  `dconvert` |  Image Scaling |  Java | [`Link`](https://github.com/patrickfav/density-converter) | [`v1.0.0.-alpha7`](misc/code/dconvert)  | [`Sample`](dashboard/resources/dconvert/sample.csv)  |[`Performance`](dashboard/resources/dconvert/measurements.csv)  | [`Option-Code`](dashboard/resources/dconvert/code/) |
|  `h2` | Database  |  Java | [`Link`](https://github.com/h2database/h2database)  |  [`v1.4.200`](misc/code/h2) |[`Sample`](dashboard/resources/h2/sample.csv) |[`Performance`](dashboard/resources/h2/measurements.csv) | [`Option-Code`](dashboard/resources/h2/code/) |
|  `batik` |  SVG Rasterizer |  Java | [`Link`](https://github.com/apache/xmlgraphics-batik)  |  [`v.1.14`](misc/code/batik) |[`Sample`](dashboard/resources/batik/sample.csv) |[`Performance`](dashboard/resources/batik/measurements.csv) | [`Option-Code`](dashboard/resources/batik/code/) |
|  `xz` | File Compessor  |  C/C++ | [`Link`](https://github.com/xz-mirror/xz)  | [`v5.2.0`](misc/code/xz)  |[`Sample`](dashboard/resources/xz/sample.csv)  |[`Performance`](dashboard/resources/xz/measurements.csv)  | [`Option-Code`](dashboard/resources/xz/code/) |
|  `lrzip` |  File Compressor | C/C++  |  [`Link`](https://github.com/ckolivas/lrzip) |  [`v0.651`](misc/code/lrzip) |[`Sample`](dashboard/resources/lrzip/sample.csv) |[`Performance`](dashboard/resources/lrzip/measurements.csv) | [`Option-Code`](dashboard/resources/lrzip/code/) |
|  `x264` |  Video Encoder | C/C++  |  [`Link`](https://github.com/mirror/x264) |  [`baee400..`](misc/code/x264) |[`Sample`](dashboard/resources/x264/sample.csv) |[`Performance`](dashboard/resources/x264/measurements.csv) | [`Option-Code`](dashboard/resources/x264/code/) |
|  `z3` | SMT Solver  |  C/C++ |  [`Link`](https://github.com/Z3Prover/z3) | [`v4.8.14`](misc/code/z3)  | [`Sample`](dashboard/resources/z3/sample.csv) |[`Performance`](dashboard/resources/z3/measurements.csv) | [`Option-Code`](dashboard/resources/z3/code/) |


### Subject Systems & Workloads (Overview)

#### `jump3r`
jump3r is a MP3 encoder written in Java. In our study, we benchmarked the execution time of its main operation, which is encoding raw WAVE audio files into the compressed MP3 format. As workloads, we used a variety of 
raw WAVE audio files from the Wikimedia Commons collection. 

#### `kanzi`
kanzi is a file compression tool which is entirely written in Java. In our study, we limited our experiments to benchmarking only to the execution time of compressing files. For this operation mode, we considered a variety of community benchmarks, community benchmarks as well as single files.

#### `dconvert`
dconvert is a utility to transform image files to formats and sizes frequently used in Android app development, i.e., different output sizes and formats. In our study, we benchmarked the execution time to scale different inputs varying in file size and type.

#### `h2`
h2 is an embedded database system which can be used as a standalone database or can be easily integrated into other applications as a library. We used different parameterizations from four widely used benchmarks (TPC-C, YCSB, Voter, SmallBank) from the OLTPBench load generator and measured the throughput over a fixed time interval of five minutes.

#### `batik`
Apache batik is a Java utility to rasterize/render SVG vector graphics. We benchmarked the execution time of rasterizing SVG files of different sizes from the Wikimedia Commons Collection.

#### `xz`
XZ is a file compression utility from the GNU software collection. Akin to kanzi, we benchmarked exeuction time for compression and the same set of workloads. In addition, we included a [synthetic benchmark](http://mattmahoney.net/dc/uiq/) to test how the software system scales with increased input size.

#### `lrzip`
Lrzip, like XZ, is a file compression utility used for compressing large files. The experimental setup in terms of worklaods and benchmarked operation mode is identical to XZ.

#### `x264`
x264 is a media transcoder which, among others, can be used to encode raw frames into video files. We selected a few test frame sets from Xiph.org varying in resolution and number, number of frames, and color depth and benchmarked the execution time it took to encode to MP4.

#### `z3`
Z3 is an open-source SMT solver by Microsoft. Its task is to prove or disprove satisfiability for SMT (satisfiability modulu theories) instances. We benchmarked the execution time it took to analyze different problem instances from the SMT2Lib competition and the regression test set provided by z3. The workloads from this competition sets mainly varied in the type of logic used. 

